const express = require("express");
const Meeting = require("../models/meeting");
const router = new express.Router();

router.get("/meeting", async (req, res) => {
  try {
    const meeting = await Meeting.find({});
    if (!meeting) {
      return res.status(404).send();
    }
    res.send(meeting);
  } catch (error) {
    res.status(500).send(e);
  }
});

router.post("/meeting", async (req, res) => {
  const meeting = new Meeting(req.body);
  try {
    await meeting.save();
    res.status(201).send(meeting);
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
