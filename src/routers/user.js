const express = require("express");
const User = require("../models/user");
const jwt = require("jsonwebtoken");
const bcrypt = require('bcryptjs')
const router = new express.Router();

// Users
router.post("/user", async (req, res) => {
  const user = new User(req.body);
  try {
    await user.save();
    const token = await user.generateAuthToken();
    res.status(201).send({ user, token });
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get("/user", async (req, res) => {
  try {
    const user = await User.find({});
    if (!user) {
      return res.status(404).send();
    }
    res.send(user);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.get("/user/:id", async (req, res) => {
  const _id = req.params.id;

  try {
    const user = await User.findById(_id);
    if (!user) {
      return res.status(404).send();
    }
    res.status(200).send(user);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.patch("/user/:id", async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ["name", "email", "password"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates" });
  }

  const _id = req.params.id;

  try {
    const user = await User.findById(_id);
    updates.forEach((update) => {
      user[update].req.body[update];
    });

    await user.save();

    if (!user) {
      res.status(404).send();
    }

    res.status(200).send(user);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete("/user/:id", async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);
    if (!user) {
      res.status(404).send();
    }
    res.status(200).send(user);
  } catch (error) {
    res.status(500).send();
  }
});

router.post("/user/login", async (req, res) => {
  console.log('here');
  try {
    // const user = await User.findByCredentials(
    //   req.body.email,
    //   req.body.password
    // );


    const user = await User.findOne({ 'email' : req.body.email });
    const isMatched = await bcrypt.compare(user.password,req.body.password)

    console.log(user);
    console.log(isMatched);





    // const token = await user.generateAuthToken();

    res.send({ user, token });
  } catch (error) {
    res.status(400).send();
  }
});

module.exports = router;
