const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken")

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error("Email is invalid");
      }
    },
  },
  password: {
    type: String,
    required: true,
  },
  tokens : [
    {
      token :{
        type:String,
        required:true
      }
    }]
});

userSchema.methods.generateAuthToken = async function()
{
  const user = this
  const token = jwt.sign({_id:user._id.toString()}, 'mysecretkey')
  user.tokens = user.tokens.concat({ token })
  await user.save()
  return token

}

userSchema.statics.findByCredentials = async function(email, password) {
  const user = await User.findOne({ 'email' : email });

  console.log(user);

  if (!user) {
    console.log('not of user');
    throw new Error("Unable to login");
  }

  // const isMatched = await bcrypt.compare(password, user.password)
  const isMatched = await bcrypt.compare(user.password,password)

  console.log(isMatched);

  if (!isMatched) {
    throw new Error("Unable to login");
  }

  return user;
};

// generate hash password
userSchema.pre("save", async function (next) {
  const user = this;

  if (user.isModified("password"));
  {
    user.password = await bcrypt.hash(user.password, 8);
  }

  next();
});


const User = mongoose.model("User", userSchema);

module.exports = User;
