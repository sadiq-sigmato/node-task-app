const mongoose = require("mongoose");
const validator = require("validator");

const Meeting = mongoose.model("Meeting", {
  title: {
    type: String,
    required: true,
  },
  image: {
    type: String,
  },
  address: {
    type: String,
  },
  description: {
    type: String,
  },
});

module.exports = Meeting;
