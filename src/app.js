const express = require("express");
require("./db/mongoose");
// Router imports
const userRouter = require("./routers/user");
const taskRouter = require("./routers/task");
const meetingRouter = require("./routers/meeting");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

app.use(userRouter);
app.use(taskRouter);
app.use(meetingRouter);

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const myfunc = async () => {
  const password = "12345678";
  const hashedPassword =
    "$2a$08$EIwsy7zrxWfrtxFX.0dsOeBmaLBgSIQ.ZEJXtQB.fNwU3gEy3K.6m";
  console.log(password);
  console.log(hashedPassword);

  const isMatch = await bcrypt.compare(password, hashedPassword);

  console.log(isMatch);
};

myfunc();

app.listen(port, () => {
  console.log("Server is up on port " + port);
});
